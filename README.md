# paper-autocomplete

> Autocomplete component compatible with Polymer 3.x



`paper-autocomplete` extends earlier efforts such as this 
[https://github.com/rodo1111/paper-input-autocomplete](https://github.com/rodo1111/paper-input-autocomplete) to provide 
keyboard support, remote binding and results scrolling.

this repo is a branch of a webcomponent published in [webcomponents.org/element/ellipticaljs/paper-autocomplete](https://www.webcomponents.org/element/ellipticaljs/paper-autocomplete) by [github.com/ellipticaljs/paper-autocomplete](https://github.com/ellipticaljs/paper-autocomplete)

# Installation

``` bash

npm install paper-autocomplete=git+https://gitlab.com/israelfloresdga/paper-autocomplete.git

```

# Usage

```js

import 'paper-autocomplete/paper-autocomplete';

 html`
  <paper-autocomplete id="my-id" label="Select" ></paper-autocomplete>
  `;

```

# Demo and Docs

http://ellipticaljs.github.io/paper-autocomplete/

**Important: The demos only work with browers which are ES2015/ES6 compatible.**. This component is compatible with older 
browsers as well, but the code need to be transpiled to ES5. `polymer build` and `polymer serve` can do that for you.
This code from this page is not transpiled.

# Want to contribute?

Check out our [Contributing guide](./CONTRIBUTING.md)! 

# For Developers

## How to run the tests

### Localy

In order to run the tests you have two option. You can either run:

```
npm run test
```

### Remote (in Saucelabs)

```
npm run test:remote
```
> You must set these two environment variables: `SAUCE_USERNAME` and `SAUCE_ACCESS_KEY` before running remote tests.


## How to *lint* the project

```
npm run lint
```
